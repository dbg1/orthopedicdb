DROP DATABASE IF EXISTS `controlin`;
CREATE DATABASE IF NOT EXISTS `controlin`;

USE `controlin`;

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente`(
`cedula` VARCHAR(10) PRIMARY KEY NOT NULL,
`nombre` VARCHAR(50) NOT NULL,
`apellido` VARCHAR(50) NOT NULL,
`direccion` VARCHAR(50),
`correo` VARCHAR(50),
`telefono` VARCHAR(20),
`ciudadResidencia` VARCHAR(50)
);

DROP TABLE IF EXISTS `proveedor`;
CREATE TABLE `proveedor`(
`idProveedor` VARCHAR(10) PRIMARY KEY NOT NULL,
`nomProveedor` VARCHAR(50) NOT NULL,
`dirProveedor` VARCHAR(50),
`telefProveedor` VARCHAR(20) NOT NULL
);

DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto`(
`codProducto` INT PRIMARY KEY NOT NULL,
`idProveedor` VARCHAR(10),
`tipoProducto` VARCHAR(50),
`nomProducto` VARCHAR(50) NOT NULL,
`precioProducto` FLOAT NOT NULL CHECK(`precioProducto` > 0.0),
`precioCosto` FLOAT NOT NULL CHECK(`precioCosto` > 0.0),
`stockProducto` INT NOT NULL CHECK(`stockProducto` > 0),
FOREIGN KEY (`idProveedor`) REFERENCES `proveedor`(`idProveedor`)
);

DROP TABLE IF EXISTS `factura`;
CREATE TABLE `factura`(
`numeroFactura` VARCHAR(50) PRIMARY KEY NOT NULL,
`fechaCompra` DATE NOT NULL,
`total` FLOAT
);

DROP TABLE IF EXISTS `venta`;
CREATE TABLE `venta`(
`numeroVenta` INT PRIMARY KEY AUTO_INCREMENT,
`cedula` VARCHAR(10),
`numeroFactura` VARCHAR(50),
`codProducto` INT NOT NULL,
`cantProducto` INT NOT NULL,
`totalVenta` FLOAT NOT NULL,
`fechaHoraVenta` DATETIME NOT NULL,
#FOREIGN KEY (`cedula`) REFERENCES `cliente`(`cedula`),
#FOREIGN KEY (`numeroFactura`) REFERENCES `factura`(`numeroFactura`),
FOREIGN KEY (`codProducto`) REFERENCES `producto`(`codProducto`)
);

DROP VIEW IF EXISTS reporteProducto;
CREATE VIEW reporteProducto(codigoProducto, nombreProveedor, nombreProducto, precioCompra, precioVenta, vecesVendido) AS 
	SELECT prod.codProducto, p.nomProveedor, prod.nomProducto, prod.precioCosto, prod.precioProducto, (SELECT SUM(v.cantProducto) FROM venta v) AS conteo
	FROM (producto prod JOIN proveedor p ON prod.idProveedor = p.idProveedor)
	JOIN venta v ON prod.codProducto = v.codProducto;
	

DROP VIEW IF EXISTS reporteProveedores;
CREATE VIEW reporteProveedores(nombreProveedor, nombreProducto, precioCompra) AS
	SELECT p.nomProveedor, prod.nomProducto, prod.precioCosto
	FROM producto prod JOIN proveedor p ON prod.idProveedor = p.idProveedor;

DROP VIEW IF EXISTS reporteClientes;
CREATE VIEW reporteClientes(nombre, codigoProducto, cantProducto, totalVenta, porcentaje) AS
	SELECT c.nombre, v.codProducto, v.cantProducto, (SELECT SUM(v.totalVenta) FROM venta v) AS totalVenta, ((v.cantProducto/(SELECT SUM(cantProducto) FROM venta))*100) AS porcentaje
	FROM (cliente c JOIN venta v ON c.cedula = v.cedula)
	JOIN factura f ON f.numeroFactura = v.numeroFactura
	GROUP BY v.cedula;

DROP VIEW IF EXISTS reporteGananciasMensuales;
CREATE VIEW reporteGananciasMensuales(mes, ventaDelMes) AS
	SELECT MONTH(fechaHoraVenta) AS mes, SUM(totalVenta) AS totalVendido
	FROM venta
	GROUP BY MONTH(fechaHoraVenta);

DROP VIEW IF EXISTS productosCompletos;
CREATE VIEW productosCompletos(codProducto, idProveedor, nomProveedor, tipoProducto, nomProducto, precioProducto, precioCosto, stockProducto) AS
	SELECT p.codProducto, p.idProveedor, pro.nomProveedor ,p.tipoProducto, p.nomProducto, p.precioProducto, p.precioCosto, p.stockProducto  
	FROM (producto p JOIN proveedor pro ON p.idProveedor=pro.idProveedor);

DROP VIEW IF EXISTS ventasCompletas;
CREATE VIEW ventasCompletas(numeroVenta, codigoProducto, nombreProducto, totalVenta, fechaHoraVenta) AS
	SELECT v.numeroVenta, v.codProducto, p.nomProducto, v.totalVenta, v.fechaHoraVenta
	FROM venta v JOIN producto p ON v.codProducto = p.codProducto;

DROP VIEW IF EXISTS facturasCompletas;
CREATE VIEW facturasCompletas(numeroFactura, cedula, nombre, correo, fechaCompra, cantidadTotalProductos ,totalFacturado) AS
	SElECT f.numeroFactura, c.cedula, c.nombre,c.correo, f.fechaCompra, (SELECT SUM(v.cantProducto) FROM venta v WHERE v.cedula = c.cedula) AS cantidadTotalProductos, f.total
	FROM (factura f JOIN venta v ON f.numeroFactura = v.numeroFactura)
	JOIN cliente c ON v.cedula = c.cedula;

INSERT INTO cliente (cedula, nombre, apellido, direccion, correo, telefono, ciudadResidencia)
VALUES('9999999999','Consumidor','final',' ',' ',' ',' '),
('0955931134', 'Elias', 'Murillo', 'Via a la costa urbanizacion Via al sol', 'elias345@hotmail.com', '2191144', 'Guayaquil'),
('0978654344', 'Jose', 'Martinez', '10 de Agosto y la 6ta', 'jma120@hotmail.com', '2345678', 'Guayaquil'),
('0989090090', 'Marta', 'Figueroa', 'Colon y la 20ava', 'mfigueroa20@yahoo.com', '2783456', 'Guayaquil'),
('0912222383', 'Elena', 'Planos', 'Urbanizacion Cataluña', 'elenaplanos1029@gmail.com', '2176567', 'Via Salitre'),
('0923234422', 'Eduardo', 'Zurita', 'Urbanizacion Bosques de la Costa', 'edzl90@gmail.com', '2209876', 'Guayaquil'),
('0978967873', 'Alex', 'Prado', 'Sucre y la 7ma', 'alexelchevere@gmail.com', '2196754', 'Guayaquil'),
('0955278272', 'Bryan', 'Escudero', 'Sauces 9', 'bryanescuderool@gmail.com', '2123498', 'Guayaquil'),
('0952728229', 'Orlando', 'Villa', 'Ciudadela la garzota', 'orlandovillabsc@hotmail.com', '2004312', 'Guayaquil'),
('0998675652', 'Jean Pierre', 'Lozano', 'Urbanizacion La joya etapa Zafiro', 'jpierrelozanom@outlook.es', '2098765', 'Daule'),
('0925262892', 'Bolivar', 'Martinez', 'Urbanizacion Villa club', 'bmartinezlopez@hotmail.com', '2126754', 'Daule'),
('0999282929', 'Roberto', 'Tenempaguay', '10 de Agosto y la 11', 'robertotm30@hotmail.com', '2189543', 'Guayaquil'),
('0952272182', 'Lenin', 'Reyes', 'Guerero Martinez y la 10', 'lenin_reyes_20@hotmail.com', '2189512', 'Guayaquil'),
('0952728224', 'Rafael', 'Acosta', 'Urbanizacion La Martina', 'rafacosta@gmail.com', '2670054', 'Duran'),
('0955927382', 'Matias', 'Camacho', 'Prosperina', 'mati_camacholo21@gmail.com', '2145326', 'Guayaquil'),
('0955927481', 'Marlon', 'Plaza', 'Sauces', 'marplaza@gmail.com', '2195326', 'Guayaquil'),
('0919927581', 'Jose', 'Pluas', 'Cristo del consuelo', 'marplaza@gmail.com', '2295326', 'Duran'),
('0923927681', 'Raul', 'Palacios', 'Ximena', 'marplaza@gmail.com', '2395326', 'Guayaquil'),
('0942927781', 'Ernesto', 'Pineda', 'Letamendi', 'marplaza@gmail.com', '2495326', 'Guayaquil'),
('0971927881', 'Francisco', 'Armendariz', 'El dorado', 'marplaza@gmail.com', '2595326', 'Guayaquil'),
('0988927981', 'Maria', 'Copiano', 'Mall del sur', 'marcopiaa@gmail.com', '2695326', 'Duran'),
('0982921081', 'Jeremy', 'Quijije', 'Villa club', 'jerqui@gmail.com', '2195376', 'Duran'),
('0992921181', 'Ariel', 'Santos', 'Base sur', 'arisantos@gmail.com', '2195386', 'Guayaquil'),
('0994921281', 'Sofia', 'Duarte', 'Centro civico', 'sofiduarte@gmail.com', '2199326', 'Duran'),
('0906921381', 'Vanessa', 'Velez', 'Las aguas', 'mvanevelez@gmail.com', '2191026', 'Guayaquil'),
('0903921481', 'Karla', 'Salao', 'Las acacias', 'karsalao@gmail.com', '2195116', 'Duran'),
('0920921581', 'Homar', 'Galarza', 'Guasmo norte', 'homherrer@gmail.com', '2112326', 'Guayaquil'),
('0926921681', 'Adriana', 'Prieto', 'Guasmo sur', 'adrianaprie@gmail.com', '2135326', 'Guayaquil'),
('0947921781', 'Victoria', 'Rojas', 'Pradera 1', 'vickyrojs@gmail.com', '2195146', 'Guayaquil'),
('0961921881', 'Sandra', 'Moran', 'Pradera 2', 'sandr_mor@gmail.com', '2195156', 'Duran'),
('0982921981', 'Lissette', 'Cevallos', 'Bloques del sur', 'lizcevallos@gmail.com', '2191626', 'Duran'),
('0993922081', 'Stefania', 'Amay', 'Las acacias', 'stefyama@gmail.com', '2191726', 'Guayaquil'),
('0924922181', 'Kevin', 'Iturralde', 'Sagrada familia', 'keviturral@gmail.com', '2118326', 'Guayaquil'),
('0916922281', 'Michael', 'Valle', 'Sagrados corazones', 'michvale99@gmail.com', '2191926', 'Guayaquil'),
('0927922381', 'Pablo', 'Robles', 'La octava y oriente', 'pablo1209@gmail.com', '2195206', 'Guayaquil'),
('0968922481', 'Israel', 'Plaza', 'La B y la 11', 'israpla09@gmail.com', '2192126', 'Guayaquil'),
('0949922581', 'Alex', 'Mota', 'Puente de la A', 'alemocr7@gmail.com', '2122326', 'Guayaquil'),
('0981922681', 'Carlos', 'Lalama', 'Lizardo garcia', 'lalacar@gmail.com', '2192326', 'Duran'),
('0941122781', 'Omar', 'Carillo', 'Garcia moreno', 'cariomar@gmail.com', '2195246', 'Guayaquil'),
('0991222881', 'Jostin', 'Alvez', 'Garcia goyena', 'josalvez@gmail.com', '2195256', 'Guayaquil'),
('0901329081', 'Ricardo', 'Paez', 'venezuela y bolivia', 'ricarpaez@gmail.com', '2126326', 'Guayaquil'),
('0945143081', 'James', 'Icaza', 'Jose de antepara y venezuela', 'jameiza@gmail.com', '2127326', 'Duran'),
('0965150981', 'Byron', 'Loor', 'Los rios y francisco segura', 'loorbyr@gmail.com', '2195286', 'Guayaquil'),
('0975163281', 'Bryan', 'Moran', 'Los olivos', 'bryan1995@gmail.com', '2192926', 'Guayaquil'),
('0995173381', 'Saul', 'Cevallos', 'La saiba', 'cevasaul@gmail.com', '2195306', 'Guayaquil'),
('0935183481', 'Jeff', 'Iturralde', 'Pradera 2', 'jefiturral@gmail.com', '2195316', 'Guayaquil'),
('0925193581', 'Jeanella', 'Valle', 'Colinas de los ceibos', 'valle2345@gmail.com', '2193226', 'Guayaquil'),
('0955203681', 'Julio', 'Pluas', 'Samborondon', 'juliopluas@gmail.com', '2193326', 'Duran'),
('0925213781', 'Jaylline', 'Crespo', 'Puntilla', 'cres_jay@gmail.com', '2195346', 'Duran'),
('0915223881', 'Dayannara', 'Limones', 'La rotonda', 'dayalim@gmail.com', '2193526', 'Guayaquil'),
('0945927280', 'Jose Alfredo', 'Macas', 'Urbanizacion Malaga II', 'joalfredo2198@hotmail.com', '2093623', 'Guayaquil');

INSERT INTO proveedor (idProveedor, nomProveedor, dirProveedor, telefProveedor)
VALUES ('0987654650', 'OrthosaLab', 'Paucarbamba, junto a la clinica Paucarbamba', '07-21814359'),
('0987654656', 'Centro Freire', 'Esmeraldas 1025 y Velez', '04-2361276'),
('0987654657', 'Ortopedia', 'Cdla Huancavilca Mz A 19 Solar 6', '04-24344600'),
('0987654655', 'Ortopedia Confort', 'Jose F de Valdiviezo 152-57 y Av Universitaria ', '07-2565574'),
('0987654658', 'Servimedic', 'Chimborazo 3310 y Azuay', '04-2334495'),
('0987654659', 'Lineas Medicas', 'Chimborazo 3403 y Azuay', '04-2440589'),
('0945324578', 'Ortopedia', 'Cdla Huancavilca Mz A 19 Solar 6', '04-2434460'),
('0987654660', 'OrthosaLab', 'Paucarbamba, junto a la clinica Paucarbamba', '07-2181439');

INSERT INTO producto(codProducto, idProveedor, tipoProducto, nomProducto, precioProducto, precioCosto, stockProducto)
VALUES (1001, '0987654650', 'rodilleras', 'rodillera01', 120.30, 100.05, 10),
(1002, '0987654656', 'Bandas', 'Banda Cabestrillo', 70.00, 60.25, 5),
(1003, '0987654657', 'coderas', 'Codera elastica', 88.00, 80.00, 9),
(1004, '0987654655', 'muñequeras', 'Muñequera Elastica', 28.00 , 25.00, 3),
(1005, '0987654658', 'Cuello','Collarines de espuma',  10.20, 9.05, 11),
(1006, '0987654658', 'Cuello', 'Collarines rigidos', 12.20, 10.05, 10),
(1007, '0987654658', 'Cuello','Collarines para extraccion', 15.20, 13.05, 10),
(1008, '0987654658', 'Cuello','Collarines postoperatorios', 17.20, 14.05, 9),
(1009, '0987654658', 'Torso','Fajas abdominales', 16.20, 14.05, 15),
(1010, '0987654658', 'Torso','Fajas toracticas', 18.20, 17.05, 14),
(1011, '0987654658', 'Columna Vertebral','Ortesis vertebrales', 21.20, 18.05, 3),
(1012, '0987654658', 'Columna Vertebral','Soporte lumbar', 5.20, 3.05, 5),
(1013, '0987654658', 'Hombro','Ortesis clavicular', 8.20, 7.05, 4),
(1014, '0987654658', 'Hombro','Inmovilizador de hombro', 7.20, 5.05, 11),
(1015, '0987654658', 'Hombro','Ortesis de abduccion del hombro', 9.20, 7.05, 18),
(1016, '0987654658', 'Codo','Ortesis postoperatoria de codo', 8.20, 6.05, 19),
(1017, '0987654658', 'Muñeca','Ferulas para muñeca', 4.20, 3.05, 18),
(1018, '0987654658', 'Muñeca','Ferulas para muñeca/antebrazo', 6.20, 5.05, 10),
(1019, '0987654658', 'Muñeca','Ferulas para muñeca y pulgar', 5.20, 3.05, 7),
(1020, '0987654658', 'Brazo','Cabestrillo', 6.20, 4.05, 6),
(1021, '0987654658', 'Brazo','Soporte elevador de brazo', 8.20, 6.05, 19),
(1022, '0987654658', 'Mano y dedos','Ferulas de pulgar', 4.20, 3.05, 17),
(1023, '0987654658', 'Mano y dedos','Muñequera de pulgar', 5.20, 3.05, 15),
(1024, '0987654658', 'Mano y dedos','Ferula dinamica de dedo con muelle', 6.20, 3.05, 5),
(1025, '0987654658', 'Mano y dedos','Ferula para dedo', 4.20, 3.05, 9),
(1026, '0987654658', 'Mano y dedos','Ferula para mano', 5.20, 2.05, 6),
(1027, '0987654658', 'Cadera','Traccion de buck', 13.20, 10.05, 8),
(1028, '0987654658', 'Rodilla','Inmovilizadores de rodilla', 7.20, 5.05, 12),
(1029, '0987654658', 'Rodilla','Estabilizador rotuliano', 14.20, 11.05, 10),
(1030, '0987654658', 'Rodilla','Rodillera', 5.20, 3.05, 9),
(1031, '0987654658', 'Rodilla','Ortesis de rodilla articulada', 6.20, 4.05, 6),
(1032, '0987654658', 'Rodilla','Ortesis postoperatorias de rodilla', 8.20, 6.05, 7),
(1033, '0987654658', 'Tobillo','Ortesis funcional de tobillo', 6.20, 4.05, 5),
(1034, '0987654658', 'Tobillo','Estribo para tobillo', 7.20, 4.05, 9),
(1035, '0987654658', 'Tobillo','Tobillera para deporte', 5.20, 3.05, 15),
(1036, '0987654658', 'Tobillo','Tobillera', 4.20, 3.05, 4),
(1037, '0987654658', 'Tobillo','Ortesis de pie y tobillo', 8.20, 6.05, 10),
(1038, '0987654658', 'Tobillo','Contractura de tobillo', 10.20, 8.05, 12),
(1039, '0987654658', 'Pie','Botas walker', 43.20, 32.05, 9),
(1040, '0987654658', 'Pie','Calzado portoperatorio', 34.20, 30.05, 5),
(1041, '0987654658', 'Pie','Zapato para escayola', 45.20, 40.05, 6),
(1042, '0987654658', 'Pie','Ferula nocturna', 42.20, 32.05, 9),
(1043, '0987654658', 'Pie','Talonera', 9.20, 8.05, 10),
(1044, '0987654658', 'Pie', 'Talonera color rojo', 8.20, 7.05, 14),
(1045, '0987654658', 'Pie', 'Talonera color negro', 5.20, 5.05, 13),
(1046, '0987654658', 'Pie','Zapato para escayola transparente', 43.20, 33.05, 7),
(1047, '0987654658', 'Topico', 'Mertiolate', 1.50, 1.00, 20),
(1048, '0987654658', 'Topico', 'Curitas rectas', 3.20, 1.05, 15),
(1049, '0987654658', 'Topico', 'Curitas circulares', 2.20, 1.05, 20),
(1050, '0987654659', 'sillas', 'silla medica', 115.45, 100.10, 9);

INSERT INTO factura(numeroFactura, fechaCompra, total)
VALUES ('9999999','2018-12-31', 0),
('0000101', '2018-10-21', 70.0),
('0000102', '2018-10-21', 88.0),
('0000103', '2018-10-23', 56.0),
('0000104', '2018-10-26', 40.2),
('0000105', '2018-10-28', 120.30),
('0000106', '2018-10-09', 115.45),
('0000107', '2018-11-09', 88.0),
('0000108', '2018-11-11', 176.0),
('0000109', '2018-11-11', 120.3),
('0000110', '2018-11-26', 70.0),
('0000111', '2018-11-27', 88.0),
('0000112', '2018-11-15', 56.0),
('0000113', '2018-11-18', 40.2),
('0000114', '2018-11-18', 230.90),
('0000115', '2018-11-27', 70.0),
('0000116', '2018-11-21', 115.45),
('0000117', '2018-12-23', 88.0),
('0000118', '2018-12-26', 176.0),
('0000119', '2018-12-28', 120.3),
('0000120', '2018-12-09', 70.0),
('0000121', '2018-12-09', 70.0),
('0000122', '2018-12-11', 88.0),
('0000123', '2018-12-11', 56.0),
('0000124', '2018-12-26', 40.2),
('0000125', '2018-12-27', 120.3),
('0000126', '2018-12-15', 115.45),
('0000127', '2018-12-18', 88.0),
('0000128', '2018-12-18', 176.0),
('0000129', '2018-12-27', 120.0),
('0000130', '2018-12-21', 70.0),
('0000131', '2018-12-23', 70.0),
('0000132', '2018-12-26', 88.0),
('0000133', '2018-12-28', 56.0),
('0000134', '2018-12-09', 40.2),
('0000135', '2018-12-09', 120.3),
('0000136', '2018-12-11', 115.45),
('0000137', '2018-12-11', 88.0),
('0000138', '2018-12-26', 176.0),
('0000139', '2018-12-27', 120.3),
('0000140', '2018-12-15', 70.0),
('0000141', '2018-12-18', 70.0),
('0000142', '2018-12-18', 88.0),
('0000143', '2018-12-27', 56.0),
('0000144', '2018-12-18', 40.2),
('0000145', '2018-12-27', 120.30),
('0000146', '2018-12-21', 115.45),
('0000147', '2018-12-23', 88.0),
('0000148', '2018-12-26', 176.0),
('0000149', '2018-12-28', 120.3),
('0000150', '2018-12-09', 70.0);
				  
INSERT INTO venta(numeroVenta, cedula, numeroFactura, codProducto, cantProducto, totalVenta, fechaHoraVenta)
VALUES (101, '0955931134', '0000101', 1002, 1, 70.00, '2018-10-21 11:00:12'),
(102, '0978654344', '0000102', 1003, 1, 88.00, '2018-10-21 11:00:12');


#Indice para ordenar los clientes por la ciudad y luego por nombre
CREATE INDEX cliendeAlfabetico ON cliente(ciudadResidencia, nombre);

#Indice para relacionar la cedula del proveedor y el nombre
CREATE INDEX proveedorNom ON proveedor(idProveedor, nomProveedor);

#Indice para diferenciar los productos por tipo
CREATE UNIQUE INDEX tipoP ON producto(codProducto,tipoProducto,nomProducto);

#Indice para ordenar las ventas por cliente
CREATE INDEX indexV ON venta(numeroVenta, cedula);

#Indice para ordenar los numeros de factura
CREATE INDEX indexF ON factura(numeroFactura);

DROP PROCEDURE IF EXISTS REGISTRAR_PRODUCTO;
DELIMITER $
CREATE PROCEDURE REGISTRAR_PRODUCTO(IN codProducto1 INT, IN idProveedor1 VARCHAR(10), IN tipoProducto1 VARCHAR(50), IN nomProducto1 VARCHAR(50), IN precioProducto1 FLOAT, IN precioCosto1 FLOAT, IN stockProducto1 INT)
	BEGIN
		IF NOT EXISTS (SELECT codProducto FROM producto WHERE codProducto=codProducto1) THEN
			INSERT INTO producto(codProducto, idProveedor, tipoProducto, nomProducto, precioProducto, precioCosto, stockProducto) VALUES (codProducto1, idProveedor1, tipoProducto1, nomProducto1, precioProducto1, precioCosto1, stockProducto1);
		END IF;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS MODIFICAR_PRODUCTO;
DELIMITER $
CREATE PROCEDURE MODIFICAR_PRODUCTO(IN codProducto1 INT, IN idProveedor1 VARCHAR(10), IN tipoProducto1 VARCHAR(50), IN nomProducto1 VARCHAR(50), IN precioProducto1 FLOAT, IN precioCosto1 FLOAT, IN stockProducto1 INT)
	BEGIN
	UPDATE producto SET codProducto=codProducto1,  idProveedor=idProveedor1, tipoProducto=tipoProducto1, nomProducto=nomProducto1, precioProducto=precioProducto1, precioCosto=precioCosto1, stockProducto=stockProducto1 WHERE codProducto=codProducto1;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS ELIMINAR_PRODUCTO;
DELIMITER $
CREATE PROCEDURE ELIMINAR_PRODUCTO(IN codProducto1 INT)
	BEGIN
		DELETE FROM producto WHERE codProducto=codProducto1 ; 
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS BUSCAR_PRODUCTO;
DELIMITER $
CREATE PROCEDURE BUSCAR_PRODUCTO(IN codProducto1 INT)
	BEGIN
		SELECT codProducto, idProveedor, tipoProducto, nomProducto, precioProducto, precioCosto, stockProducto FROM producto WHERE codProducto=codProducto1;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS REGISTRAR_CLIENTE;
DELIMITER $
CREATE PROCEDURE REGISTRAR_CLIENTE(IN cedula1 VARCHAR(10), IN nombre1 VARCHAR(50), IN apellido1 VARCHAR(50), IN correo1 VARCHAR(50), IN telefono1 VARCHAR(20))
	BEGIN
		IF NOT EXISTS (SELECT cedula, nombre, apellido, correo, telefono FROM cliente WHERE cedula = cedula1) THEN
			INSERT INTO cliente(cedula, nombre, apellido, correo, telefono) VALUES (cedula1, nombre1, apellido1, correo1, telefono1);
		END IF;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS MODIFICAR_CLIENTE;
DELIMITER $
CREATE PROCEDURE MODIFICAR_CLIENTE(IN cedula1 VARCHAR(10), IN nombre1 VARCHAR(50), IN apellido1 VARCHAR(50), IN correo1 VARCHAR(50), IN telefono1 VARCHAR(20))
	BEGIN
		UPDATE cliente SET cedula=cedula1, nombre=nombre1, apellido=apellido1, correo=correo1, telefono=telefono1 WHERE cedula = cedula1;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS ELIMINAR_CLIENTE;
DELIMITER $
CREATE PROCEDURE ELIMINAR_CLIENTE(IN cedula1 VARCHAR(10))
	BEGIN
		DELETE FROM venta WHERE numeroFactura = (SELECT numeroFactura FROM factura WHERE cedula = cedula1);
		DELETE FROM factura WHERE cedula = cedula1;
		DELETE FROM cliente WHERE cedula = cedula1;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS BUSCAR_CLIENTE;
DELIMITER $
CREATE PROCEDURE BUSCAR_CLIENTE(IN cedula1 VARCHAR(10))
	BEGIN
		SELECT cedula, nombre, apellido, correo, telefono FROM cliente WHERE cedula = cedula1;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS AGREGAR_VENTA;
DELIMITER $
CREATE PROCEDURE AGREGAR_VENTA(IN cedula1 VARCHAR(10), IN numeroFactura1 VARCHAR(50), IN codProducto1 INT, IN cantidad1 INT, IN precioProducto1 FLOAT)
	BEGIN
		DECLARE llave INT;
		DECLARE total1 FLOAT;
        SET llave = (SELECT numeroVenta FROM venta ORDER BY numeroVenta DESC LIMIT 1) + 1;
		SET total1 = (precioProducto1 * cantidad1);
		IF ((cedula1 IS NULL) AND (numeroFactura1 IS NULL)) THEN
			UPDATE venta SET total = total + total1 WHERE numeroFactura = '9999999';
		ELSE 
			INSERT INTO venta(numeroVenta, cedula, numeroFactura, codProducto, cantProducto, totalVenta, fechaHoraVenta) VALUES (llave, cedula1, numeroFactura1, codProducto1, cantidad1, total1, NOW());			
		END IF;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS FACTURAR;
DELIMITER $
CREATE PROCEDURE FACTURAR(IN numeroFactura1 VARCHAR(50), IN total1 FLOAT)
	BEGIN
		IF numeroFactura1 IS NULL THEN
			UPDATE factura SET total = total + total1 WHERE numeroFactura = '9999999';
		ELSE
			INSERT INTO factura VALUES (numeroFactura1, NOW(), total1);
		END IF;
	END $
DELIMITER ;


DROP PROCEDURE IF EXISTS BUSCAR_FACTURA;
DELIMITER $
CREATE PROCEDURE BUSCAR_FACTURA(IN cedula1 VARCHAR(10))
	BEGIN
		SELECT numeroFactura, cedula, correo, fechaCompra, cantidadTotalProductos , totalFacturado FROM facturasCompletas WHERE cedula = cedula1;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS BUSCAR_PRODUCTO_VENTA;
DELIMITER $
CREATE PROCEDURE BUSCAR_PRODUCTO_VENTA(IN codProducto1 INT)
	BEGIN
		SELECT codProducto, nomProducto, precioProducto FROM producto WHERE codProducto=codProducto1;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS REGISTRAR_PROVEEDOR;
DELIMITER $
CREATE PROCEDURE REGISTRAR_PROVEEDOR(IN idProveedor1 VARCHAR(10), IN nomProveedor1 VARCHAR(50), IN dirProveedor1 VARCHAR(50), IN telefProveedor1 VARCHAR(20))
	BEGIN
		IF NOT EXISTS (SELECT idProveedor, nomProveedor, dirProveedor, telefProveedor FROM proveedor WHERE idProveedor = idProveedor1) THEN
			INSERT INTO proveedor(idProveedor, nomProveedor, dirProveedor, telefProveedor) VALUES (idProveedor1, nomProveedor1, dirProveedor1, telefProveedor1);
		END IF;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS MODIFICAR_PROVEEDOR;
DELIMITER $
CREATE PROCEDURE MODIFICAR_PROVEEDOR(IN idProveedor1 VARCHAR(10), IN nomProveedor1 VARCHAR(50), IN dirProveedor1 VARCHAR(50), IN telefProveedor1 VARCHAR(20))
	BEGIN
	UPDATE cliente SET idProveedor=idProveedor1, nomProveedor=nomProveedor1, dirProveedor=dirProveedor1, telefProveedor=telefProveedor1 WHERE idProveedor = idProveedor1;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS ELIMINAR_PROVEEDOR;
DELIMITER $
CREATE PROCEDURE ELIMINAR_PROVEEDOR(IN idProveedor1 VARCHAR(10))
	BEGIN
		DELETE FROM proveedor WHERE idProveedor = idProveedor1;
	END$
DELIMITER ;

DROP PROCEDURE IF EXISTS BUSCAR_PROVEEDOR;
DELIMITER $
CREATE PROCEDURE BUSCAR_PROVEEDOR(IN idProveedor1 VARCHAR(10))
	BEGIN
		SELECT idProveedor, nomProveedor, dirProveedor, telefProveedor FROM proveedor WHERE idProveedor = idProveedor1;
	END$
DELIMITER ;


DROP TRIGGER IF EXISTS CONTROLAR_STOCK;
DELIMITER $
CREATE TRIGGER CONTROLAR_STOCK 
AFTER INSERT ON venta 
FOR EACH ROW
	BEGIN
		DECLARE cant INT;
	    SET cant = (SELECT cantProducto FROM venta ORDER BY numeroVenta DESC LIMIT 1);
		IF cant >=1 THEN 
			UPDATE producto SET stockProducto = (stockProducto - cant) WHERE codProducto = (SELECT codigoProducto FROM venta ORDER BY numeroVenta DESC LIMIT 1);
	    END IF;
END$
DELIMITER ;

DROP TRIGGER IF EXISTS FECHA_ACTUAL_VENTA;
DELIMITER $
CREATE TRIGGER FECHA_ACTUAL_VENTA
	AFTER INSERT ON venta
    FOR EACH ROW
    BEGIN
		UPDATE venta SET fechaHoraVenta = now() WHERE fechaHoraVenta = (SElECT fechaHoraVenta FROM venta ORDER BY numeroVenta DESC LIMIT 1);
	END$
DELIMITER ;

DROP TRIGGER IF EXISTS FECHA_ACTUAL_FACTURA;
DELIMITER $
CREATE TRIGGER FECHA_ACTUAL_FACTURA
	AFTER INSERT ON factura
    FOR EACH ROW
    BEGIN
		UPDATE factura SET fechaCompra = now() WHERE fechaCompra = (SElECT fechaCompra FROM factura ORDER BY numeroFactura DESC LIMIT 1);
	END$
DELIMITER ;

DROP USER IF EXISTS 'Lourdes'@'Localhost';
CREATE USER 'Lourdes'@'Localhost' IDENTIFIED BY 'lourdes1';
DROP USER IF EXISTS 'Johan'@'Localhost';
CREATE USER 'Johan'@'Localhost' IDENTIFIED BY 'admin';
DROP USER IF EXISTS 'Adriana'@'Localhost';
CREATE USER 'Adriana'@'Localhost' IDENTIFIED BY '1230';
DROP USER IF EXISTS 'Alexis'@'Localhost';
CREATE USER 'Alexis'@'Localhost' IDENTIFIED BY '1230';
DROP USER IF EXISTS 'Jeffrey'@'Localhost';
CREATE USER 'Jeffrey'@'Localhost' IDENTIFIED BY '1230';

GRANT ALL PRIVILEGES ON controlin TO 'Adriana'@'Localhost' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON controlin TO 'Alexis'@'Localhost' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON controlin TO 'Jeffrey'@'Localhost' WITH GRANT OPTION;

GRANT INSERT, SELECT, UPDATE, DELETE ON controlin.* TO 'Lourdes'@'Localhost' WITH GRANT OPTION;
GRANT INSERT, SELECT, UPDATE ON controlin.* TO 'Johan'@'Localhost';

GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_PRODUCTO TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_PRODUCTO TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_PRODUCTO TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PRODUCTO TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_CLIENTE TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_CLIENTE TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_CLIENTE TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_CLIENTE TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.AGREGAR_VENTA TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PRODUCTO_VENTA TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.FACTURAR TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_FACTURA TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_PROVEEDOR TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_PROVEEDOR TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_PROVEEDOR TO 'Lourdes'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PROVEEDOR TO 'Lourdes'@'Localhost';

GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_PRODUCTO TO 'Johan'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_PRODUCTO TO 'Johan'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_CLIENTE TO 'Johan'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_CLIENTE TO 'Johan'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PRODUCTO TO 'Johan'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.AGREGAR_VENTA TO 'Johan'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PRODUCTO_VENTA TO 'Johan'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_PROVEEDOR TO 'Johan'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_PROVEEDOR TO 'Johan'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_PROVEEDOR TO 'Johan'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PROVEEDOR TO 'Johan'@'Localhost';

GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_PRODUCTO TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_PRODUCTO TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_PRODUCTO TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PRODUCTO TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_CLIENTE TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_CLIENTE TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_CLIENTE TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_CLIENTE TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.AGREGAR_VENTA TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PRODUCTO_VENTA TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.FACTURAR TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_FACTURA TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_PROVEEDOR TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_PROVEEDOR TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_PROVEEDOR TO 'Adriana'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PROVEEDOR TO 'Adriana'@'Localhost';

GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_PRODUCTO TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_PRODUCTO TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_PRODUCTO TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PRODUCTO TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_CLIENTE TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_CLIENTE TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_CLIENTE TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_CLIENTE TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.AGREGAR_VENTA TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PRODUCTO_VENTA TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.FACTURAR TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_FACTURA TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_PROVEEDOR TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_PROVEEDOR TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_PROVEEDOR TO 'Alexis'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PROVEEDOR TO 'Alexis'@'Localhost';

GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_PRODUCTO TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_PRODUCTO TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_PRODUCTO TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PRODUCTO TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_CLIENTE TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_CLIENTE TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_CLIENTE TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_CLIENTE TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.AGREGAR_VENTA TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PRODUCTO_VENTA TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.FACTURAR TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_FACTURA TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.REGISTRAR_PROVEEDOR TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.MODIFICAR_PROVEEDOR TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.ELIMINAR_PROVEEDOR TO 'Jeffrey'@'Localhost';
GRANT EXECUTE ON PROCEDURE controlin.BUSCAR_PROVEEDOR TO 'Jeffrey'@'Localhost';

GRANT SELECT ON controlin.reporteProducto TO 'Lourdes'@'Localhost';
GRANT SELECT ON controlin.reporteProveedores TO 'Lourdes'@'Localhost';
GRANT SELECT ON controlin.reporteClientes TO 'Lourdes'@'Localhost';
GRANT SELECT ON controlin.reporteGananciasMensuales TO 'Lourdes'@'Localhost';
GRANT SELECT ON controlin.productosCompletos TO 'Lourdes'@'Localhost';
GRANT SELECT ON controlin.ventasCompletas TO 'Lourdes'@'Localhost';
GRANT SELECT ON controlin.facturasCompletas TO 'Lourdes'@'Localhost';

GRANT SELECT ON controlin.reporteProducto TO 'Johan'@'Localhost';
GRANT SELECT ON controlin.reporteClientes TO 'Johan'@'Localhost';
GRANT SELECT ON controlin.productosCompletos TO 'Johan'@'Localhost';
GRANT SELECT ON controlin.ventasCompletas TO 'Johan'@'Localhost';
GRANT SELECT ON controlin.facturasCompletas TO 'Johan'@'Localhost';

GRANT SELECT ON controlin.reporteProducto TO 'Adriana'@'Localhost';
GRANT SELECT ON controlin.reporteProveedores TO 'Adriana'@'Localhost';
GRANT SELECT ON controlin.reporteClientes TO 'Adriana'@'Localhost';
GRANT SELECT ON controlin.reporteGananciasMensuales TO 'Adriana'@'Localhost';
GRANT SELECT ON controlin.productosCompletos TO 'Adriana'@'Localhost';
GRANT SELECT ON controlin.ventasCompletas TO 'Adriana'@'Localhost';
GRANT SELECT ON controlin.facturasCompletas TO 'Adriana'@'Localhost';

GRANT SELECT ON controlin.reporteProducto TO 'Alexis'@'Localhost';
GRANT SELECT ON controlin.reporteProveedores TO 'Alexis'@'Localhost';
GRANT SELECT ON controlin.reporteClientes TO 'Alexis'@'Localhost';
GRANT SELECT ON controlin.reporteGananciasMensuales TO 'Alexis'@'Localhost';
GRANT SELECT ON controlin.productosCompletos TO 'Alexis'@'Localhost';
GRANT SELECT ON controlin.ventasCompletas TO 'Alexis'@'Localhost';
GRANT SELECT ON controlin.facturasCompletas TO 'Alexis'@'Localhost';

GRANT SELECT ON controlin.reporteProducto TO 'Jeffrey'@'Localhost';
GRANT SELECT ON controlin.reporteProveedores TO 'Jeffrey'@'Localhost';
GRANT SELECT ON controlin.reporteClientes TO 'Jeffrey'@'Localhost';
GRANT SELECT ON controlin.reporteGananciasMensuales TO 'Jeffrey'@'Localhost';
GRANT SELECT ON controlin.productosCompletos TO 'Jeffrey'@'Localhost';
GRANT SELECT ON controlin.ventasCompletas TO 'Jeffrey'@'Localhost';
GRANT SELECT ON controlin.facturasCompletas TO 'Jeffrey'@'Localhost';